<?php

defined('APP_PATH') or exit('No direct script access allowed.');

function response($type, $message, $data = []) {
    return json_encode([
        'type' => $type,
        'message' => $message,
        'data' => json_encode($data)
    ]);
}

function getHeaderKey() {
    return isset(getallheaders()['X-Token']) ? getallheaders()['X-Token'] : null;
}

function createJWT($payload) {
    $header = encode(json_encode([
        'typ' =>'JWT',
        'alg' => 'HS256'
    ]));
    $payload = encode(json_encode($payload));
    $signature = encode(encrypt($header . '.' . $payload));
    return $header . '.' . $payload . '.' . $signature;
}

function readJWT($jwt) {
    $payload = json_decode(decode(explode('.', $jwt)[1]), true);
    if($jwt == createJWT($payload)) {
        return $payload;
    } else {
        return false;
    }
}

function encode($data) {
    $data = base64_encode($data);
    $data = str_replace(['+','/','='], ['-','_',''], $data);
    return $data;
}

function decode($data) {
    $data = str_replace(['-','_'], ['+','/'], $data);
    $data = base64_decode($data);
    return $data;
}

function encrypt($data) {
    return hash_hmac('sha256', $data, APP_SECRET);
}

function token($length) {
    return bin2hex(random_bytes($length));
}

function uuid() {
    $rnd = encrypt(token(mt_rand(1, 100)) . microtime(true));
    $random = '';
    for($i = 0; $i < strlen($rnd); $i += 2) {
        $random .= chr(hexdec($rnd[$i] . $rnd[$i + 1]));
    }
    $hash = encrypt($rnd . $random . token(mt_rand(1, 100)) . microtime(true));
    return sprintf('%08s-%04s-%04x-%04x-%12s',
        substr($hash, 0, 8),
        substr($hash, 8, 4),
        (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,
        (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,
        substr($hash, 20, 12)
    );
}

function createCookie($name, $value, $timeout) {
    $name = encode(encrypt($name));
    $value = encode($value);
    $timeout = empty($timeout) ? 0 : $timeout + time();
    setcookie($name, $value, $timeout, '/', false, false, false);
}

function readCookie($name) {
    $name = encode(encrypt($name));
    return empty($_COOKIE[$name]) ? false : decode($_COOKIE[$name]);
}

function deleteCookie($name) {
    $name = encode(encrypt($name));
    setcookie($name, null, time() - 3600, '/', false, false, false);
    unset($_COOKIE[$name]);
}

function createSession($name, $value) {
    if(session_status() == PHP_SESSION_NONE) {
        $id = encode(encrypt(session_id()));
        session_id($id);
        session_start();
    }
    $name = encode(encrypt($name));
    $value = encode($value);
    $_SESSION[$name] = $value;
}

function readSession($name) {
    if(session_status() == PHP_SESSION_NONE) {
        $id = encode(encrypt(session_id()));
        session_id($id);
        session_start();
    }
    $name = encode(encrypt($name));
    return empty($_SESSION[$name]) ? false : decode($_SESSION[$name]);
}

function deleteSession() {
    if(session_status() == PHP_SESSION_NONE) {
        $id = encode(encrypt(session_id()));
        session_id($id);
        session_start();
    }
    $name = session_name();
    session_unset();
    session_destroy();
    setcookie($name, null, time() - 3600, '/', false, false, false);
    unset($_COOKIE[$name]);
}

function createCache($name, $value, $timeout) {
    $file = path('/src/caches/cach_' . encode(encrypt($name)));
    $value = encode($value);
    $value = serialize(['timeout' => $timeout + time(), 'value' => $value]);
    file_put_contents($file, $value);
}

function readCache($name) {
    $file = path('/src/caches/cach_' . encode(encrypt($name)));
    if(file_exists($file)) {
        $value = unserialize(file_get_contents($file));
        if($value['timeout'] > time()) {
            return decode($value['value']);
        } else {
            unlink($file);
        }
    }
    return false;
}

function upload($files, $filter) {
    foreach($files as $file) {
        if(empty($file)) {
            return false;
        }
        $extension = pathinfo($file['name'], PATHINFO_EXTENSION);
        if(!in_array($extension, $filter)) {
            return false;
        }
        $upload = path('/src/uploads/uplo_' . encode(encrypt(uuid())) . '.' . $extension);
        if(!move_uploaded_file($file['tmp_name'], $upload)) {
            return false;
        }
        return $upload;
    }
}

function curl($options) {
    try {
        $curl = curl_init();
        curl_setopt_array($curl, $options);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    } catch(Exception $e) {
        exit($e->getMessage());
    } 
}

function mailer($to, $subject, $body) {
    try {
        $mailer = new \PHPMailer\PHPMailer\PHPMailer(false);
        $mailer->isSMTP();
        $mailer->SMTPAuth = true;
        $mailer->CharSet = 'UTF-8';
        $mailer->Host = SMTP_HOST;
        $mailer->Username = SMTP_USERNAME;
        $mailer->Password = SMTP_PASSWORD;
        $mailer->SMTPSecure = 'ssl';
        $mailer->Port = 465;
        $mailer->addCustomHeader('X-Priority: 1');
        $mailer->addCustomHeader('X-MSMail-Priority: High');
        $mailer->setFrom(SMTP_USERNAME, APP_TITLE);
        $mailer->addAddress($to);
        $mailer->isHTML(true);
        $mailer->Subject = $subject;
        $mailer->Body = $body;
        return $mailer->send();
    } catch(Exception $e) {
        exit($e->getMessage());
    } 
}

function database($query, $table, $params) {
    try {
        $connection = new \PDO(sprintf('%s:host=%s;dbname=%s;port=%s;charset=utf8;', DB_DRIVER, DB_HOST, DB_NAME, DB_PORT), DB_USERNAME, DB_PASSWORD);
        $connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $stmt = $connection->prepare(sprintf($query, $table));
        $stmt->execute($params);
        return $stmt;
    } catch(Exception $e) {
        exit($e->getMessage());
    }
}

function minify($file) {
    try {
        $minifier = strpos($file, '.css') ? new \MatthiasMullie\Minify\CSS($file) : new \MatthiasMullie\Minify\JS($file);
        return $minifier->minify();
    } catch(Exception $e) {
        exit($e->getMessage());
    }
}

function view($view, $params) {
    try {
        extract($params, EXTR_OVERWRITE);
        $output = null;
        ob_start();
        include path('/src/views/' . $view . '.php');
        $output = ob_get_contents();
        ob_end_clean();
        $htmlMin = new \voku\helper\HtmlMin();
        $htmlMin->doOptimizeViaHtmlDomParser();
        $htmlMin->doRemoveComments();
        $htmlMin->doSumUpWhitespace();
        $htmlMin->doRemoveWhitespaceAroundTags();
        $htmlMin->doOptimizeAttributes();
        $htmlMin->doRemoveHttpPrefixFromAttributes();
        $htmlMin->doRemoveDefaultAttributes();
        $htmlMin->doRemoveDeprecatedAnchorName();
        $htmlMin->doRemoveDeprecatedScriptCharsetAttribute();
        $htmlMin->doRemoveDeprecatedTypeFromScriptTag();
        $htmlMin->doRemoveDeprecatedTypeFromStylesheetLink();
        $htmlMin->doRemoveEmptyAttributes();
        $htmlMin->doRemoveValueFromEmptyInput();
        $htmlMin->doSortCssClassNames();
        $htmlMin->doSortHtmlAttributes();
        $htmlMin->doRemoveSpacesBetweenTags();
        $htmlMin->doRemoveOmittedQuotes();
        $htmlMin->doRemoveOmittedHtmlTags();
        return $htmlMin->minify($output);
    } catch(Exception $e) {
        exit($e->getMessage());
    }
}

function redirect($url) {
    exit(header('Location: ' . $url, true, 301));
}

function path($path) {
    return APP_PATH . $path;
}

function host($host) {
    return APP_HOST . $host;
}
