<?php

namespace mthsena\src\repositories;

defined('APP_PATH') or exit('No direct script access allowed.');

class Images {

    private $table = 'images';

    public function create($bird, $url) {
        $query = 'insert into %s (bird, url) values (?, ?)';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? false : true;
    }

    public function readAllByBird($bird) {
        $query = 'select * from %s where bird = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

}
