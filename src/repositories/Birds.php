<?php

namespace mthsena\src\repositories;

defined('APP_PATH') or exit('No direct script access allowed.');

class Birds {

    private $table = 'birds';

    public function create($account, $category, $race, $color, $gender, $status, $birthDate, $cage, $rightRing, $leftRing, $registry, $name, $mother, $father, $image) {
        $query = 'insert into %s (account, category, race, color, gender, status, birth_date, cage, right_ring, left_ring, registry, name, mother, father, image) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? false : true;
    }

    public function read($id) {
        $query = 'select * from %s where id = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetch(\PDO::FETCH_ASSOC);
    }

    public function readAllByAccount($account) {
        $query = 'select * from %s where account = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function readChildren($birdId1, $birdId2) {
        $query = 'select * from %s where father = ? or mother = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? [] : $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function update($account, $category, $race, $color, $gender, $status, $birthDate, $cage, $rightRing, $leftRing, $registry, $name, $mother, $father , $image, $id) {
        $query = 'update %s set account = ?, category = ?, race = ?, color = ?, gender = ?, status = ?, birth_date = ?, cage = ?, right_ring = ?, left_ring = ?, registry = ?, name = ?, mother = ?, father = ?, image = ? where id = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? false : true;
    }

    public function updateImage($image, $id) {
        $query = 'update %s set image = ? where id = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? false : true;
    }

    public function delete($id) {
        $query = 'delete from %s where id = ?';
        $result = database($query, $this->table, func_get_args());
        return empty($result->rowCount()) ? false : true;
    }
}
