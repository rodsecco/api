<?php

defined('APP_PATH') or exit('No direct script access allowed.');

return [
    ['POST', '/account/auth', function($params) { new \mthsena\src\controllers\account\Auth($params); } ],
    ['POST', '/account/create', function($params) { new \mthsena\src\controllers\account\Create($params); } ],
    ['POST', '/account/read', function($params) { new \mthsena\src\controllers\account\Read($params); } ],
    ['POST', '/account/recovery-send', function($params) { new \mthsena\src\controllers\account\RecoverySend($params); } ],
    ['POST', '/account/recovery-change', function($params) { new \mthsena\src\controllers\account\RecoveryChange($params); } ],

    ['POST', '/bird/create', function($params) { new \mthsena\src\controllers\bird\Create($params); } ],
    ['POST', '/bird/read', function($params) { new \mthsena\src\controllers\bird\Read($params); } ],
    ['POST', '/bird/read-all-by-account', function($params) { new \mthsena\src\controllers\bird\ReadAllByAccount($params); } ],
    ['POST', '/bird/read-children', function($params) { new \mthsena\src\controllers\bird\ReadChildren($params); } ],
    ['POST', '/bird/update', function($params) { new \mthsena\src\controllers\bird\Update($params); } ],
    ['POST', '/bird/delete', function($params) { new \mthsena\src\controllers\bird\Delete($params); } ],
    ['POST', '/bird/create-image', function($params) { new \mthsena\src\controllers\bird\CreateImage($params); } ],
    ['POST', '/bird/read-image', function($params) { new \mthsena\src\controllers\bird\ReadImage($params); } ],

    ['POST', '/cage/create', function($params) { new \mthsena\src\controllers\cage\Create($params); } ],
    ['POST', '/cage/read', function($params) { new \mthsena\src\controllers\cage\Read($params); } ],
    ['POST', '/cage/read-all-by-account', function($params) { new \mthsena\src\controllers\cage\ReadAllByAccount($params); } ],
    ['POST', '/cage/update', function($params) { new \mthsena\src\controllers\cage\Update($params); } ],
    ['POST', '/cage/delete', function($params) { new \mthsena\src\controllers\cage\Delete($params); } ],

    ['POST', '/baby/create', function($params) { new \mthsena\src\controllers\baby\Create($params); } ],
    ['POST', '/baby/read', function($params) { new \mthsena\src\controllers\baby\Read($params); } ],
    ['POST', '/baby/read-all-by-cage', function($params) { new \mthsena\src\controllers\baby\ReadAllByCage($params); } ],
    ['POST', '/baby/update', function($params) { new \mthsena\src\controllers\baby\Update($params); } ],
    ['POST', '/baby/delete', function($params) { new \mthsena\src\controllers\baby\Delete($params); } ],

    ['POST', '/egg/create', function($params) { new \mthsena\src\controllers\egg\Create($params); } ],
    ['POST', '/egg/read', function($params) { new \mthsena\src\controllers\egg\Read($params); } ],
    ['POST', '/egg/read-all-by-cage', function($params) { new \mthsena\src\controllers\egg\ReadAllByCage($params); } ],
    ['POST', '/egg/update', function($params) { new \mthsena\src\controllers\egg\Update($params); } ],
    ['POST', '/egg/delete', function($params) { new \mthsena\src\controllers\egg\Delete($params); } ],
];
