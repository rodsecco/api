<?php

namespace mthsena\src\controllers\baby;

defined('APP_PATH') or exit('No direct script access allowed.');

class Delete {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $babyRepository = new \mthsena\src\repositories\Babies();
        $id = isset($params['post']['id']) ? $params['post']['id'] : false;
        if(!$id) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $babyRepository->delete($id);
        exit(response('success', 'O filhote foi deletado com sucesso!'));
    }

}
