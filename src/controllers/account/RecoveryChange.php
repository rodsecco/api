<?php

namespace mthsena\src\controllers\account;

defined('APP_PATH') or exit('No direct script access allowed.');

class RecoveryChange {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $accountRepository = new \mthsena\src\repositories\Accounts();
        $email = isset($params['post']['email']) ? strtolower($params['post']['email']) : false;
        $code = isset($params['post']['code']) ? $params['post']['code'] : false;
        $password = isset($params['post']['password']) ? encrypt($email . $params['post']['password']) : false;
        if(!$email || !$code || !$password) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $account = $accountRepository->read($email);
        if(empty($account)) {
            exit(response('danger', 'A conta não foi encontrada.'));
        }
        if($code != $account['password']) {
            exit(response('danger', 'Código inválido.'));
        }
        $accountRepository->updatePassword($password, $email);
        exit(response('success', 'Sua senha foi alterada com sucesso!'));
    }

}
