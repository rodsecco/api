<?php

namespace mthsena\src\controllers\account;

defined('APP_PATH') or exit('No direct script access allowed.');

class Read {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $accountRepository = new \mthsena\src\repositories\Accounts();
        $babyRepository = new \mthsena\src\repositories\Babies();
        $eggRepository = new \mthsena\src\repositories\Eggs();
        $email = isset($params['post']['email']) ? strtolower($params['post']['email']) : false;
        if(!$email) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $account = $accountRepository->read($email);
        if(empty($account)) {
            exit(response('danger', 'A conta não foi encontrada.'));
        }
        $account['babies'] = $babyRepository->readAllByAccount($account['id']);;
        $account['eggs'] = $eggRepository->readAllByAccount($account['id']);
        exit(response('success', 'A conta foi obtida com sucesso!', $account));
    }

}
