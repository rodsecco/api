<?php

namespace mthsena\src\controllers\bird;

defined('APP_PATH') or exit('No direct script access allowed.');

class ReadAllByAccount {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $birdRepository = new \mthsena\src\repositories\Birds();
        $account = isset($params['post']['account']) ? $params['post']['account'] : false;
        if(!$account) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $birds = $birdRepository->readAllByAccount($account);
        if(empty($birds)) {
            exit(response('danger', 'As aves não foram encontradas.'));
        }
        exit(response('success', 'As aves foram obtidas com sucesso!', $birds));
    }

}
