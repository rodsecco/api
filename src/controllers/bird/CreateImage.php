<?php

namespace mthsena\src\controllers\bird;

defined('APP_PATH') or exit('No direct script access allowed.');

class CreateImage {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $imageRepository = new \mthsena\src\repositories\Images();
        $birdRepository = new \mthsena\src\repositories\Birds();
        $bird = isset($params['post']['bird']) ? $params['post']['bird'] : false;
        $base64 = isset($params['post']['base64']) ? $params['post']['base64'] : false;
        if(!$bird || !$base64) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $fileName = '/src/uploads/uplo_' . encrypt(uuid()) . '.' . 'jpg';
        file_put_contents(APP_PATH . $fileName, base64_decode($base64));
        $imageRepository->create($bird, host($fileName));
        $birdData = $birdRepository->read($bird);
        if($birdData['image'] == host('/src/uploads/bird.png')) {
            $birdRepository->updateImage(host($fileName), $bird);
        }
        exit(response('success', 'A imagem foi adicionada com sucesso!'));
    }

}
