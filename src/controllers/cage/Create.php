<?php

namespace mthsena\src\controllers\cage;

defined('APP_PATH') or exit('No direct script access allowed.');

class Create {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $cageRepository = new \mthsena\src\repositories\Cages();
        $account = isset($params['post']['account']) ? $params['post']['account'] : false;
        $state = isset($params['post']['state']) ? $params['post']['state'] : false;
        $name = isset($params['post']['name']) ? $params['post']['name'] : '';
        if(!$account || !$state) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $cageRepository->create($account, $state, $name);
        exit(response('success', 'A gaiola foi criada com sucesso!'));
    }

}
