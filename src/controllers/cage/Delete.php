<?php

namespace mthsena\src\controllers\cage;

defined('APP_PATH') or exit('No direct script access allowed.');

class Delete {

    public function __construct($params) {
        $isPost = $params['method'] == 'POST';
        $isSigned = getHeaderKey() == APP_SECRET;
        if($isPost && $isSigned) {
            $this->post($params);
        } else {
            http_response_code(404);
            exit('404 Not Found. The page you requested does not exist or has been moved.');
        }
    }

    private function post($params) {
        $cageRepository = new \mthsena\src\repositories\Cages();
        $id = isset($params['post']['id']) ? $params['post']['id'] : false;
        if(!$id) {
            exit(response('warning', 'Preencha todos os campos corretamente.'));
        }
        $cageRepository->delete($id);
        exit(response('success', 'A gaiola foi deletada com sucesso!'));
    }

}
